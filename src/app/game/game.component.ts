import {Component} from '@angular/core';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent {
  numbers: number[] = [7, 12, 19, 22, 30];

  generateNumbers(event: Event) {
    event.preventDefault();
    let crystalValues = [];
    for(let i = 0;i < 5;i++){
      let randomNumber = Math.floor(Math.random() * 32) + 5;
      if (crystalValues.indexOf(randomNumber) === -1) {
        crystalValues.push(randomNumber);
      } else {
        i--;
      }
    }
    this.numbers = crystalValues.sort(function(a, b){return a - b});
  }
}
